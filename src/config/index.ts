type InterfaceConfig = {
  http: {
    port: number,
    cors: {
      origin: string[],
      credentials: boolean
    },
    api: {
      path: string,
      version: number,
      modules: {
        [key: string]: {
          route: string,
          auth: boolean
        }
      }
    }
  }
}

type InfrastructureConfig = {
  jwt: {
    secret: string,
    expiresIn: string
  },
  email: {
    ses: {
      key: string,
      secret: string,
      amazon: string
    }
  },
  database: {
    neo4j?: {
      host: string,
      port: string,
      username: string,
      password: string
    }
  },
  servicebus: {
    url: string
  }
}

export type AppConfig = InterfaceConfig & InfrastructureConfig & {
  env: string,
  version: string,
}

export default require(`./${(process.env.NODE_ENV || 'development')}`).default as AppConfig
