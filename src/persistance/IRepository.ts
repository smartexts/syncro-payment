export type FindOptions = {
  filter: object
}

export type Relation = {
  collection: string,
  rel: string,
  direction: 'IN' | 'OUT'
}

export type IRepository = {
  relations: { [key: string]: Relation }
  findOne: (payload) => Promise<any>
  find: (options: FindOptions) => Promise<any[]>
  create: (values) => Promise<any>
  update: (values: object) => Promise<any>
  saveRelate: (values: object, relates: { [key: string]: Relation }) => Promise<any[]>
}

export type IRepositoryDB = {
  findOne: (collection, payload) => Promise<any>
  find: (collection, payload: FindOptions) => Promise<any[]>
  create (collection: string, attributes: object): Promise<any>
  update (collection: string, params: object | string, attributes: object): Promise<any>
  createRelate (from: { node: string, params: object | string }, to: { node: string, params: object | string }, relate: Relation): Promise<any>
}
