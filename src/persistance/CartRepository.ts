
import { BaseRepository } from './BaseRepository'
import { Neo4jQuery } from '../infrastructure/neo4j/query'

class CartRepository extends BaseRepository {
  relations = {
    owner: {
      node: 'User',
      rel: 'CREATED_CART',
      direction: 'IN',
    }
  }

  constructor (protected db: Neo4jQuery) {
    super(db, 'Cart')
  }
}

export default ({ database }) => {
  return new CartRepository(database.neo4j)
}
