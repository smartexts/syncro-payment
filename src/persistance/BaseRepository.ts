import { IRepository, FindOptions, IRepositoryDB } from './IRepository'

export abstract class BaseRepository implements IRepository {
  relations = {}

  constructor (protected db: IRepositoryDB, protected node) {
  }

  findOne (payload) {
    return this.db.findOne(this.node, payload)
  }

  find (payload: FindOptions) {
    return this.db.find(this.node, payload)
  }

  async create (values) {
    const attributes = this._extractAttributes(values)
    const relations = this._extractRelations(values)

    const result = await this.db.create(this.node, attributes)

    return await this.saveRelate(result, relations)
  }

  async update (values) {
    const attributes = this._extractAttributes(values)
    const relations = this._extractRelations(values)

    const result = await this.db.update(this.node, { id: values.id }, attributes)

    return await this.saveRelate(result, relations)
  }

  async saveRelate (result, relates) {
    for (const key of Object.keys(relates)) {
      const relation = this.relations[key]

      result.key = await this.db.createRelate(
        { node: this.node, params: { id: result.id } },
        { node: relation.node, params: relates[key] },
        this.relations[key])
    }
    return result
  }

  _extractAttributes (data) {
    return Object.keys(data).filter(key => !this._isRelation(key)).reduce((obj, k) => {
      obj[k] = data[k]
      return obj
    }, {})
  }

  _extractRelations (data: object) {
    return Object.keys(data).filter(key => this._isRelation(key))
      .reduce((obj, k) => {
        obj[k] = data[k]
        return obj
      }, {})
  }

  _isRelation (key) {
    return !!this.relations[key]
  }

}
