
import { BaseRepository } from './BaseRepository'
import { comparePassword } from '../infrastructure/encryption'
import { IRepositoryDB } from './IRepository'

class UserRepository extends BaseRepository {
  constructor (protected db: IRepositoryDB) {
    super(db, 'User')
  }

  findByUsername (username) {
    return this.findOne({ username })
  }

  validatePassword (hash, salt) {
    return (password) => comparePassword(password, { hash, salt })
  }

}

export default ({ database }) => {
  return new UserRepository(database.neo4j.db)
}
