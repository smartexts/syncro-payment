import { EventStore, EventStoreRepository } from '../../modules/core/infrastructure'

export const makeClient = (entityName, reducer, table?) => {
  return (eventstore) => {
    const client = new EventStore(entityName, eventstore, table)
    return new EventStoreRepository(client, reducer)
  }
}
