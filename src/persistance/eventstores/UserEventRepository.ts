
import { makeClient } from './makeClient'
import { reduceToUser } from '../../modules/user/domain/entities/reducerToUser'

export default ({ database }) => {
  return makeClient('User', reduceToUser, 'EventStores')(database.neo4j.eventstore)
}
