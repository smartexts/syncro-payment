import * as t from 'io-ts'
import { UserId } from '../input'
import { Event } from '../../../../core/domain/input'

export const UserEvent = t.union([
  t.intersection([
    Event,
    t.type({
      type: t.literal('UserCreated'),
      payload: t.type({
        userId: UserId,
      })
    })
  ])
])
