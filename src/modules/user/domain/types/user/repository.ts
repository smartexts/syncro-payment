import { User, UserEvent, UserId } from './index'

export type UserRepository = {
  getById: (userId: UserId) => Promise<{
    userState: User,
    save: (events: UserEvent[], version?: number) => Promise<any>
  }>
  create: (userId: UserId) => {
    save: (events: UserEvent[]) => Promise<any>
  }
}
