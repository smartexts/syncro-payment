import * as t from 'io-ts'
import { brand, Email } from '../../../../core/domain/input'

export const UserId = brand(t.string, 'UserId')

export const Gender = t.union([
  t.literal('male'),
  t.literal('female'),
])

export const UserInfo = t.partial({
  id: UserId,
  email: Email
})
