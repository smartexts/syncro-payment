
import { UserId } from '../index'

export type User = {
  userId: UserId
}
