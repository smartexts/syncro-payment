import { type } from 'io-ts'
import { Command } from '../../../../core/domain/input'

export const publicCommands = type({
  CreateUser: Command
})
