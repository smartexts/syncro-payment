import { TypeOf } from 'io-ts'
import * as input from './input'
import * as commands from './commands'
import { UserEvent as UserEventIo } from './events'

// input types
export type UserId = TypeOf<typeof input.UserId>
export type Gender = TypeOf<typeof input.Gender>

// command types
export type PublicUserCommands = TypeOf<typeof commands.publicCommands>
export type UserCommands = PublicUserCommands

// event types
export type UserEvent = TypeOf<typeof UserEventIo>

// model types
export * from './model'

// repository types
export * from './repository'
