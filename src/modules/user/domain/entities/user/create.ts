import { UserEvent, UserId } from '../../types'
import { Timestamp } from '../../../../core/domain'

export function createUser (
  userId: UserId,
  timestamp: Timestamp,
): UserEvent[] {

  return [
    {
      type: 'UserCreated',
      userId,
      payload: {
        userId,
      },
      timestamp
    },
  ]
}
