import { UserEvent, User } from '../../domain/types'

const initialState: User = {
  userId: null
}

export const userReducer = (state: User = initialState, e: UserEvent): User => {
  switch (e.type) {
    case 'UserCreated':
      return {
        ...state,
        userId: e.payload.userId,
      }
  }

  return state
}

export const reduceToUser = (userHistory: UserEvent[], initialState?: User): User => userHistory.reduce(userReducer, initialState)
