import { Module } from '../core/domain'
import { userCommandHandlers } from './commands/user'
import * as userCommands from './domain/types/user/commands'

export default ({ eventstores }): Module => {
  return {
    handlers: {
      ...userCommandHandlers(eventstores.User),
    },
    commands: {
      ...userCommands.publicCommands.props,
    }
  }
}
