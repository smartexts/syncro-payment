import * as t from '../domain/types'

import { createUser } from '../domain/entities/user/create'

type CommandHandlers = {
  [C in keyof t.UserCommands]: (
    command: t.UserCommands[C]
  ) => Promise<any>
}

export const userCommandHandlers = (
  userRepository: t.UserRepository,
): CommandHandlers => ({

  CreateUser: async ({ userId, timestamp }) => {

    let exist = false
    try {
      await userRepository.getById(userId)
      exist = true
    } catch (error) {
      // console.log(error)
    }

    if (exist) {
      throw new Error('User Exist')
    }

    const { save } = userRepository.create(userId)

    return save(
      createUser(
        userId,
        timestamp
      )
    )
  },
})
