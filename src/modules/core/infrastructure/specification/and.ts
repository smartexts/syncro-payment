import ISpecification from './interface'
import Specification from './abstract'

export class AndSpecification extends Specification {
  private a: ISpecification
  private b: ISpecification

  constructor (a: ISpecification, b: ISpecification) {
    super()
    this.a = a
    this.b = b
  }

  isSatisfiedBy (candidate): boolean {
    return this.a.isSatisfiedBy(candidate) && this.b.isSatisfiedBy(candidate)
  }
}
