import ISpecification from './interface'
import Specification from './abstract'

export class AndNotSpecification extends Specification {
  public a: ISpecification
  public b: ISpecification

  constructor (a: ISpecification, b: ISpecification) {
    super()
    this.a = a
    this.b = b
  }

  public isSatisfiedBy (candidate) {
    return this.a.isSatisfiedBy(candidate) && !this.b.isSatisfiedBy(candidate)
  }
}
