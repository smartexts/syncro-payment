export default interface ISpecification {
  isSatisfiedBy (candidate: ISpecification): boolean
  and (candidate: ISpecification): ISpecification
  andNot (candidate: ISpecification): ISpecification
  or (candidate: ISpecification): ISpecification
  orNot (candidate: ISpecification): ISpecification
  not (): ISpecification
}
