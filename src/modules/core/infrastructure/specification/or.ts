import ISpecification from './interface'
import Specification from './abstract'

export class OrSpecification extends Specification {
  private a: ISpecification
  private b: ISpecification

  constructor (a: ISpecification, b: ISpecification) {
    super()
    this.a = a
    this.b = b
  }

  public isSatisfiedBy (candidate) {
    return this.a.isSatisfiedBy(candidate) || this.b.isSatisfiedBy(candidate)
  }
}
