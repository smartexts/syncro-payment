import ISpecification from './interface'
import Specification from './abstract'

export class NotSpecification extends Specification {
  private wrapper: ISpecification

  constructor (wrapper) {
    super()
    this.wrapper = wrapper
  }

  public isSatisfiedBy (candidate) {
    return !this.wrapper.isSatisfiedBy(candidate)
  }
}
