import ISpecification from './interface'
import { AndSpecification } from './and'
import { AndNotSpecification } from './andNot'
import { OrSpecification } from './or'
import { OrNotSpecification } from './orNot'
import { NotSpecification } from './not'

export default abstract class Specification implements ISpecification {
  public isSatisfiedBy (candidate: ISpecification): boolean {
    return candidate.isSatisfiedBy(candidate)
  }

  public and (candidate: ISpecification): ISpecification {
    return new AndSpecification(this, candidate)
  }

  public andNot (candidate: ISpecification): ISpecification {
    return new AndNotSpecification(this, candidate)
  }

  public or (candidate: ISpecification): ISpecification {
    return new OrSpecification(this, candidate)
  }

  public orNot (candidate: ISpecification): ISpecification {
    return new OrNotSpecification(this, candidate)
  }

  public not (): ISpecification {
    return new NotSpecification(this)
  }
}
