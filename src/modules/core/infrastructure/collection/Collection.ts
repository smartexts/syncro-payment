export class Collection extends Map {
  filter (callback) {
    const items = new Collection()
    this.forEach((value, key) => {
      if (callback(value, key)) {
        items.set(key, value)
      }
    })

    return items
  }

  sum (attr: string): number {
    return this.reduce((total, item) => {
      total += item[attr]
      return total
    }, 0)
  }

  reduce (callback, memo) {
    this.forEach((v) => {
      memo = callback(memo, v)
    })
    return memo
  }

  toArray () {
    return Array.from(this.values())
  }
}
