import { IEventStoreRepository, IEventStore, Event } from '../../domain'

export class EventStoreRepository<T extends IEventStore>implements IEventStoreRepository<any> {
  protected _eventStore: T
  protected _reducer: Function

  constructor (eventStore: T, reducer: Function) {
    this._eventStore = eventStore
    this._reducer = reducer
  }

  getById (entityId: string): Promise<{ state: any, save: (events: Event[], version?: number) => Promise<any> }> {
    return this._eventStore.load(entityId).then((history: any) => ({
      state: this._reducer(history.events),
      save: events => this._eventStore.append(entityId, history.version, events)
    }))
  }

  create (entityId: string): { save: (events: Event[]) => Promise<any> } {
    return {
      save: events => this._eventStore.append(entityId, 0, events)
    }
  }

  get store (): T {
    return this._eventStore
  }
}
