import { IEventStore, IEventStoreDB, EventStoreCommit, Event } from '../../domain'

export class EventStore<T extends IEventStoreDB> implements IEventStore {
  protected _entityName: string
  protected _table: string
  protected _db: T

  constructor (entityName, db, table = 'eventsourcing') {
    this._entityName = entityName
    this._table = table
    this._db = db
  }

  async load (entityId): Promise<{ events: Event[], version: number }> {
    const history = await this._db.load(this._table, { entityId, entityName: this._entityName })

    if (history.length) {
      return { events: this.getEntityEvents(history), version: history.length }
    } else {
      throw new Error(`${this._entityName}:${entityId} not found`)
    }
  }

  getEntityEvents (history: EventStoreCommit[] = []): Event[] {
    let items: Event[] = []

    history.sort((a, b) => (a.version - b.version)).forEach(item => {
      const commitedEvents = item.events
      if (commitedEvents.length) {
        commitedEvents.forEach(event => {
          items.push(event)
        })
      }
    })

    return items
  }

  async append (entityId, version, events): Promise<EventStoreCommit> {
    const commit: EventStoreCommit = {
      entityId,
      entityName: this._entityName,
      version,
      events: events
    }

    return this._db.append(this._table, commit)
  }

  get db (): T {
    return this._db
  }

  get entityName (): string {
    return this._entityName
  }
}
