import * as t from 'io-ts'

// helper function for creating nominal type in TS
// by using intersection with { __brand: Type }
// https://github.com/Microsoft/TypeScript/issues/202
export function brand<T, B extends string> (
  type: t.Type<any, T>,
  _brand: B
): t.Type<any, T & { __brand: B }> {
  return type as any
}
