import { Event } from '..'

export type EventStoreCommit = {
  entityId: string
  entityName: string
  version: number
  events: Event[]
}

export type IEventStoreDB = {
  load (entityId: string, filter: object): Promise<any[]>
  append (entityId: string, data: any): Promise<any>
}

export type IEventStore = {
  load (entityId): Promise<{ events: Event[], version: number }>
  append (entityId, version, events): Promise<EventStoreCommit>
}
