import { Event } from '..'

export type IEventStoreRepository<T> = {
  getById: (entityId: string) => Promise<{
    state: T,
    save: (events: Event[], version?: number) => Promise<any>
  }>
  create: (entityId: string) => {
    save: (events: Event[]) => Promise<any>
  }
}
