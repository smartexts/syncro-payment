import * as t from 'io-ts'
import * as isUrl from 'is-url'
import { brand } from '../helper'

export const NonNegativeInt = t.refinement(t.Integer, num => num >= 0)
export const String100 = t.refinement(t.string, s => s.length <= 100)
export const Email = t.refinement(t.string, () => /* todo: validation! */ true)
export const After2017 = t.refinement(t.Integer, val => val > 1483228800)
export const Timestamp = brand(After2017, 'Timestamp')
export const URL = brand(t.refinement(t.string, isUrl), 'URL')
export const UUID = t.refinement(t.string, s => s.length <= 36)
