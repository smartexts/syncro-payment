import { TypeOf, Type } from 'io-ts'
import * as input from './input'
import { Command } from '../command'

export type URL = TypeOf<typeof input.URL>
export type Timestamp = TypeOf<typeof input.Timestamp>
export type Email = TypeOf<typeof input.Email>
export type UUID = TypeOf<typeof input.UUID>

export type Module = {
  handlers: {
    [key: string]: (command: Command) => Promise<any>
  },
  commands: {
    [key: string]: Type<any>
  }
}
