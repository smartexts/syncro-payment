import { TypeOf } from 'io-ts'
import * as input from './input'

export type Command = TypeOf<typeof input.Command>
export type CommandInput = TypeOf<typeof input.CommandInput>
export type Event = TypeOf<typeof input.Event>
