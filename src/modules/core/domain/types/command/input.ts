import * as t from 'io-ts'
import { Timestamp } from '../common/input'

export const Command = t.intersection([
  t.type({
    userId: t.string,
    timestamp: Timestamp
  }),
  t.partial({
    payload: t.Dictionary,
  })
])

export const Event = t.intersection([
  t.type({
    type: t.string,
  }),
  Command
])

export const CommandInput = t.intersection([
  t.type({
    name: t.string,
  }),
  Command
])
