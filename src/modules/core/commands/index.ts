import { validateCommand } from './validate'
import { EventStoreCommit, CommandInput, Module } from '../domain'

export type ResolvedResult = Promise<EventStoreCommit>
export type ResolverHandler = {
  handle: (command: CommandInput, middlewares?: ResolverHandlerMiddleware[]) => ResolvedResult
  validate: (command: CommandInput) => string[]
  hasCommand: (command: CommandInput) => boolean
}

export type CommandResolver = (command: CommandInput) => ResolvedResult

export type ResolverHandlerMiddleware = (handler: ComposeResolverHandler) => (next: CommandResolver) => (command: CommandInput) => ResolvedResult

export function applyMiddlewares (handler: ComposeResolverHandler, resolver: CommandResolver, middlewares: ResolverHandlerMiddleware[] = []): CommandResolver {
  if (middlewares.length) {

    const _middlewares = middlewares.slice()
    _middlewares.reverse()

    let next = resolver
    _middlewares.forEach(middleware =>
      next = middleware(handler)(next)
    )
    return next
  }
  return resolver
}

export class ComposeResolverHandler implements ResolverHandler {
  handlers = {}
  commands = {}
  middlewares: ResolverHandlerMiddleware[] = []

  constructor (modules: Module[], middlewares: ResolverHandlerMiddleware[] = []) {
    modules.forEach(module => {
      this.handlers = { ...this.handlers, ...module.handlers }
      this.commands = { ...this.commands, ...module.commands }
    })

    this.middlewares = middlewares
  }

  addMiddleware (middleware: ResolverHandlerMiddleware) {
    this.middlewares.push(middleware)
    return this
  }

  validate = (command: CommandInput): string[] => {
    if (!this.hasCommand(command)) {
      return ['Command not supported']
    }
    return validateCommand(this.commands, command) || []
  }

  hasCommand = (command: CommandInput): boolean => {
    return !!this.commands[command.name]
  }

  async handle (command: CommandInput, middlewares: ResolverHandlerMiddleware[] = []) {
    const errors = this.validate(command)
    if (errors.length) {
      throw new Error(JSON.stringify(errors))
    }

    return await applyMiddlewares(this, this.handlers[command.name], [...this.middlewares, ...middlewares])(command)
  }

}

export default ({ modules }: { modules: Module[] }): ResolverHandler => {
  return new ComposeResolverHandler(modules)
}
