import { createContainer, asValue, asFunction, Resolver } from 'awilix'

// App
import config from './config'
import app from './app'
import events from './infrastructure/eventEmitter'
import database from './infrastructure/database'
import jwt from './infrastructure/jwt'
import logger from './infrastructure/logging'
import email from './infrastructure/email'
import servicebus from './infrastructure/servicebus'
import middlewares from './infrastructure/middlewares'
import gensync from './infrastructure/gensync'
import paypal from './infrastructure/paypal'
import eventsourcing from './infrastructure/eventsourcing'

// Interfaces
import server from './interfaces/http/server'
import router from './interfaces/http/router'
import auth from './interfaces/http/auth'

// Module handler
import handler from './app/command'

// Modules
import userModule from './modules/user'

// Eventstores
import UserEventRepository from './persistance/eventstores/UserEventRepository'

// Repositories
import UserRepository from './persistance/UserRepository'
import CartRepository from './persistance/CartRepository'
import { BaseRepository } from './persistance/BaseRepository'

// SYSTEM
const container = createContainer()

container
  .register({
    config: asValue(config),
    app: asFunction(app).singleton(),
    email: asFunction(email).singleton(),
    logger: asFunction(logger).singleton(),
    database: asFunction(database).singleton(),
    jwt: asFunction(jwt).singleton(),
    events: asFunction(events).singleton(),
    servicebus: asFunction(servicebus).singleton(),
    gensync: asFunction(gensync).singleton(),
    paypal: asFunction(paypal).singleton(),
    eventsourcing: asFunction(eventsourcing).singleton(),
    server: asFunction(server).singleton(),
    auth: asFunction(auth).singleton(),
    router: asFunction(router).singleton(),
    // Modules
    handler: asFunction(handler).singleton(),
    modules: asArray([
      asFunction(userModule).singleton(),
    ]),
    middlewares: asFunction(middlewares),
    eventstores: asObject({
      User: asFunction(UserEventRepository),
    }),
    repositories: asObject<BaseRepository>({
      User: asFunction(UserRepository),
      Cart: asFunction(CartRepository),
    }),
  })

// Helper function to register array of modules
function asArray<T> (resolvers: Resolver<T>[]): Resolver<T[]> {
  return {
    resolve: c => resolvers.map(r => r.resolve(c))
  }
}

function asObject<T> (resolvers: { [key: string]: Resolver<T> }): Resolver<{ [key: string]: T }> {
  return {
    resolve: c => Object.keys(resolvers).reduce((obj, key) => {
      obj[key] = resolvers[key].resolve(c)
      return obj
    }, {})
  }
}

export default container
