import { App } from './domain/types'
import container from './container'

const app: App = container.resolve('app')
const logger: any = container.resolve('logger')

app
  .start()
  .catch((error) => {
    logger.error(error)
    process.exit()
  })
