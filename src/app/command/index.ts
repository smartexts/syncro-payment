import { Module } from '../../modules/core/domain'
import { ResolverHandler, ComposeResolverHandler, ResolverHandlerMiddleware } from '../../modules/core/commands'

export default ({ modules, middlewares }: { modules: Module[], middlewares: ResolverHandlerMiddleware[] }): ResolverHandler => new ComposeResolverHandler(modules, middlewares)
