export default ({ server, logger }) => {
  return {
    start: () =>
      Promise
        .resolve()
        .then(server.start)
        .catch((e: Error) => {
          logger.error(e.message)
        })
  }
}
