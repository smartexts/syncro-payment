/**
 * this file will hold all the get use-case for user domain
 */
import Token from '../../domain/token'

/**
 * function for getter user.
 */
export default ({ userRepository, webToken }) => {
  // code for getting all the items
  const validate = ({ body }) => new Promise(async (resolve, reject) => {
    try {
      const credentials = new Token(body.email, body.password)

      const userCredentials = await userRepository.findOne({ email: credentials.email })

      if (!userCredentials) {
        throw new Error('Invalid Credentials')
      }

      const validatePass = await userRepository.validatePassword(userCredentials.password, userCredentials.saft)

      if (!validatePass(credentials.password)) {
        throw new Error('Invalid Credentials')
      }
      const signIn = webToken.signin()

      resolve({
        token: signIn({
          id: userCredentials.id,
          username: userCredentials.username,
          email: userCredentials.email,
        }),
      })
    } catch (error) {
      reject({ message: error.message })
    }
  })

  return {
    validate,
  }
}
