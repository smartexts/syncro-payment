import * as express from 'express'
import * as path from 'path'

export default ({
  config,
  router,
  logger,
  auth,
}) => {
  const app = express()

  app.disable('x-powered-by')
  app.use(auth.initialize())
  app.use(auth.session())
  app.use(router)

  // we define our static folder
  app.use('/', express.static(path.join(process.cwd(), 'public')))

  return {
    app,
    start: () => new Promise(() => {
      const http = app.listen(config.http.port, () => {
        // @ts-ignore
        const { port } = http.address()
        logger.info(`🤘 API - Port ${port}`)
      })

    })
  }
}
