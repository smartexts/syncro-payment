import * as cors from 'cors'
import * as bodyParser from 'body-parser'
import { Router } from 'express'
import * as modules from './modules'

export default ({ config, auth, logger }) => {
  const router = Router()

  router
    .use(cors(config.http.cors))
    .use(bodyParser.json())

  try {
    const apiRoute = Router()

    const moduleConfig = config.http.api.modules || {}

    Object.keys(modules).forEach(key => {
      const config = moduleConfig[key]

      if (config) {
        if (config.auth) {
          apiRoute.use('/' + config.route, auth.authenticate(), modules[key]())
        } else {
          apiRoute.use('/' + config.route, modules[key]())
        }
      }
    })

    router.use('/' + config.http.api.path + '/v' + config.http.api.version, apiRoute)

  } catch (error) {
    logger.error(error)
  }

  return router
}
