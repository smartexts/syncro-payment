import * as passport from 'passport'
import { ExtractJwt, Strategy } from 'passport-jwt'

/**
 * middleware to check the if auth vaid
 */

export default ({ config, repositories }) => {
  const params = {
    secretOrKey: config.jwt.secret,
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('jwt')
  }

  const strategy = new Strategy(params, (payload, done) => {
    repositories.User.findOne({ email: payload.email })
      .then((user) => {
        done(null, user)
      })
      .catch((error) => done(error, null))
  })

  passport.use(strategy)

  passport.serializeUser((user, done) => {
    done(null, user)
  })

  passport.deserializeUser((user, done) => {
    done(null, user)
  })

  return {
    initialize: () => passport.initialize(),
    session: () => passport.session(),
    authenticate: () => passport.authenticate('jwt')
  }
}
