import * as Status from 'http-status'
import { Router } from 'express'

import container from '../../../../container'

import { create } from '../../../../app/token'

export default () => {

  const router = Router()
  const {
    logger,
    jwt,
    repositories,
  } = container.cradle
  const postUseCase = create({
    userRepository: repositories.User,
    webToken: jwt,
  })

  router
    .post('/', (req, res) => {
      postUseCase
        .validate({ body: req.body })
        .then((data) => {
          res.status(Status.OK).json(data)
        })
        .catch((error) => {
          logger.error(error)
          res.status(Status.BAD_REQUEST).json(error)
        })
    })

  return router
}
