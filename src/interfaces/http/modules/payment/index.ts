import * as Status from 'http-status'
import { Router } from 'express'

import container from '../../../../container'
import PayPal from './paypal'

export default () => {
  const { paypal, gensync, eventsourcing, repositories } = container.cradle
  const router = Router()
  const paymentService = PayPal({ paypal, eventsourcing })

  router
    .get('/', (req: any, res) => {
      res.redirect('/payment/index.html')
    })
    .post('/pay', async (req, res) => {
      const payload = req.body.payload

      console.log(payload)

      if (!payload) {
        return res.status(Status.BAD_REQUEST).json({
          errors: ['Invalid input']
        })
      }

      const endpoint = [req.protocol, req.get('host')].join('://')

      if (payload.cartId) {
        const cart = await repositories.Cart.findOne(payload.cartId)

        payload.amount = {
          total: cart.total.value,
          currency: cart.total.currency
        }
      }

      const payment = {
        total: parseFloat(payload.amount.value),
        currency: payload.amount.currency,
        description: payload.description,
        endpoint
      }

      return gensync(paymentService.create)(payload, payment, payload.email)
        .then(val => {
          return res.status(Status.OK).json({
            message: 'Command accepted',
            data: val
          })
        }).catch(err => {
          if (!Array.isArray(err)) {
            err = [err]
          }

          return res.status(Status.BAD_REQUEST).json({
            errors: err.map(e => (e.response ? e.response : e.message))
          })
        })
    })
    .get('/success', (req: any, res) => {

      return gensync(paymentService.excute)(req.query.paymentId, req.query.PayerID)
        .then(val => {
          return res.status(Status.OK).json(val)
        }).catch(err => {
          if (!Array.isArray(err)) {
            err = [err]
          }

          return res.status(Status.BAD_REQUEST).json({
            errors: err.map(e => (e.response ? e.response : e.message))
          })
        })
    })
    .get('/error', (req, res) => {
      return res.status(Status.BAD_REQUEST).json({
        errors: ['Payment error'],
        data: req.query
      })
    })

  return router
}
