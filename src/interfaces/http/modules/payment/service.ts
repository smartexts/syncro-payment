export default ({ eventsourcing }) => {
  let service

  async function createCustomer () {
    try {
      await service.command({ name: 'CreateUser' })
    } catch (error) {
      // do nothing
    }
  }

  function createCart () {
    return service.command({
      name: 'CreateCart'
    })
  }

  function createPayment (payload) {
    return service.command({
      name: 'CreatePayment',
      payload: {
        invoiceNumber: payload.invoiceNumber,
        description: payload.description,
        amount: payload.amount,
        method: payload.method,
      },
    })
  }

  function addCartPayment (cartId, paymentId) {
    return service.command({
      name: 'AddCartPayment',
      payload: { cartId, paymentId }
    })
  }

  function addCartItem (cartId, amount, description) {
    return service.command({
      name: 'AddCartItem',
      payload: { cartId, amount, description, quantity: 1 }
    })
  }

  function userPay (paymentId, amount) {
    return service.command({
      name: 'UserPay',
      payload: {
        // get payment id from transaction
        'paymentId': paymentId,
        'amount': {
          value: parseFloat(amount.total),
          currency: amount.currency,
        }
      },
    })
  }

  return (user) => {
    service = eventsourcing(user)

    return {
      createCustomer,
      createCart,
      addCartItem,
      createPayment,
      addCartPayment,
      userPay
    }
  }
}
