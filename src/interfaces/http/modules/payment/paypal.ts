import EventService from './service'

export default ({ paypal, eventsourcing }) => {
  const createService = EventService({ eventsourcing })

  function* create (payload, payment, email) {
    const service = createService({ email })

    yield service.createCustomer()

    if (!payload.cartId) {
      // Create cart
      const cart = yield service.createCart()
      payload.cartId = cart.data.uuid

      // Add cart item
      yield service.addCartItem(cart.data.uuid, payload.amount, payload.description)
    }

    // Send payment command to evetnsourcing-server
    const { data } = yield service.createPayment(payload)

    // Send data to paypal to create payment
    const transaction = yield paypal.createPay(paypal.createPaymentInput({ ...payment, custom: payload.cartId + ',' + data.uuid + ',' + email }))

    let links = transaction.links
    let counter = links.length

    while (counter--) {
      if (links[counter].method === 'REDIRECT') {
        yield links[counter].href
      }
    }
  }

  function* excute (id, PayerID) {
    const details = {
      'payer_id': PayerID
    }

    const payment = yield paypal.executePay(id, details)

    const [cartId, paymentId, email] = payment.transactions[0].custom.split(',')

    const service = createService({ email })

    yield service.userPay(paymentId, payment.transactions[0].amount)

    yield service.addCartPayment(cartId, paymentId)
  }

  return {
    create,
    excute
  }
}
