import * as Status from 'http-status'
import { Router } from 'express'
import { Logger } from 'winston'

import container from '../../../../container'
import { ResolverHandler } from '../../../../modules/core/commands'

export default () => {
  const { logger, handler }: {
    logger: Logger,
    handler: ResolverHandler,
  } = container.cradle

  const router = Router()

  router
    .post('/', (req, res) => {
      const data = req.body

      if (!data || !data.command) {
        return res.status(Status.BAD_REQUEST).json({
          errors: ['Invalid command']
        })
      }

      const command = {
        name: data.command.name,
        payload: data.command.payload,
        // @ts-ignore
        userId: req.user.id,
        timestamp: Math.round(+new Date() / 1000)
      }

      const errors = handler.validate(command)

      if (errors.length) {
        logger.error(errors.join(', '))

        return res.status(Status.BAD_REQUEST).json({
          errors
        })
      }

      handler.handle(command)
        .then(r => {
          // @ts-ignore
          res.status(Status.OK).json({ message: 'command accepted', data: { ...r, uuid: r.entityId } })
        })
        .catch(e => {
          logger.error(e.message, {
            message: e.message,
            stack: e.stack,
            name: e.name,
          })
          return res.status(Status.BAD_REQUEST).json({
            errors: [e.message]
          })
        })

    })

  return router
}
