import * as paypal from 'paypal-rest-sdk'

export default ({ config }) => {
  paypal.configure({
    'mode': config.paypal.mode,
    'client_id': config.paypal.client_id,
    'client_secret': config.paypal.client_secret
  })

  const redirectUrls = (endpoint) => {
    return {
      'return_url': endpoint + config.paypal.return_url,
      'cancel_url': endpoint + config.paypal.cancel_url
    }
  }

  return {
    createPaymentInput: (data: { total: string, currency: string, description: string, custom: string, endpoint }) => ({
      'intent': 'authorize',
      'payer': {
        'payment_method': 'paypal'
      },
      'redirect_urls': redirectUrls(data.endpoint),
      'transactions': [{
        'amount': {
          total: parseFloat(data.total),
          currency: data.currency,
        },
        'description': data.description,
        'custom': data.custom,
      }]
    }),

    createPay: (payment) => {
      return new Promise((resolve, reject) => {
        paypal.payment.create(payment, function (err, payment) {
          if (err) {
            reject(err)
          } else {
            resolve(payment)
          }
        })
      })
    },

    executePay: (paymentId, details) => {
      return new Promise((resolve, reject) => {
        paypal.payment.execute(paymentId, details, function (err, payment) {
          if (err) {
            reject(err)
          } else {
            resolve(payment)
          }
        })
      })
    }
  }
}
