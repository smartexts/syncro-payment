const jwt = require('jsonwebtoken')

export default ({ config }) => ({
  signin: (options) => (payload) => {
    const opt = Object.assign({}, options, { expiresIn: config.jwt.expiresIn })
    return jwt.sign(payload, config.jwt.secret, opt)
  },
  verify: (options) => (token) => {
    const opt = Object.assign({}, options)
    return jwt.verify(token, config.jwt.secret, opt)
  },
  decode: (options) => (token) => {
    const opt = Object.assign({}, options)
    return jwt.verify(token, opt)
  }
})
