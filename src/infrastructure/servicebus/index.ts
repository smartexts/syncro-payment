import * as servicebus from 'servicebus'
import * as retry from 'servicebus-retry'

export default ({ config }) => {
  return servicebus
    .bus(config.servicebus)
    .use(retry({
      store: new retry.MemoryStore()
    }))
}
