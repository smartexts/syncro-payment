import * as fs from 'fs'
import { createLogger, format, transports } from 'winston'

export default ({ config }) => {

  if (!fs.existsSync('logs')) {
    fs.mkdirSync('logs')
  }

  const enumerateErrorFormat = format(info => {
    // @ts-ignore
    if (info.message instanceof Error) {
      info.message = Object.assign({
        message: info.message.message,
        stack: info.message.stack
      }, info.message)
    }

    if (info instanceof Error) {
      return Object.assign({
        message: info.message,
        stack: info.stack
      }, info)
    }

    return info
  })

  const _path = (type) => {
    return `logs/${config.env}.${type}.log`
  }

  const logger = createLogger({
    level: config.env === 'development' ? 'debug' : 'info',
    format: format.combine(
      enumerateErrorFormat(),
      format.timestamp(),
      format.json()
    ),
    transports: [
      new transports.File({ filename: _path('error'), level: 'error' }),
      new transports.File({ filename: _path('combined') })
    ],
    exceptionHandlers: [
      new transports.File({
        filename: _path('exceptions')
      })
    ]
  })

  if (config.env === 'development') {
    logger.add(new transports.Console({
      format: format.simple()
    }))
  }

  return logger
}
