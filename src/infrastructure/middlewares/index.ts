import { ResolverHandlerMiddleware } from '../../modules/core/commands'
import EventBus from './eventBus'
import InjectHandler from './injectHandler'
import CommitToState from './commitToState'

export default ({ logger, eventstores, servicebus }): ResolverHandlerMiddleware[] => {
  return [
    CommitToState({ eventstores }),
    EventBus({ logger, servicebus }),
    InjectHandler()
  ]
}
