import { ComposeResolverHandler, CommandResolver } from '../../modules/core/commands'
import { CommandInput, EventStoreCommit, Event } from '../../modules/core/domain'

export default ({ logger, servicebus }) => {
  // @ts-ignore
  return (handler: ComposeResolverHandler) => (next: CommandResolver) => async (command: CommandInput): Promise<EventStoreCommit> => {

    const result = await next(command)

    const _events: Event[] = result.events
    if (_events.length) {
      for (const e of _events) {
        // @ts-ignore
        servicebus.publish('events.' + e.type, e,)
      }
    }

    return result
  }
}
