import { ComposeResolverHandler, CommandResolver } from '../../modules/core/commands'
import { CommandInput, EventStoreCommit } from '../../modules/core/domain'

export default ({ eventstores }) => {
  // @ts-ignore
  return (handler: ComposeResolverHandler) => (next: CommandResolver) => async (command: CommandInput): Promise<EventStoreCommit> => {
    const result: EventStoreCommit = await next(command)

    let repo = eventstores[result.entityName]
    if (repo) {
      const entity = await repo.getById(result.entityId)
      // @ts-ignore
      result.state = entity.state
    }

    return result
  }
}
