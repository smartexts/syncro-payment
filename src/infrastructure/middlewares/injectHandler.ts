import { ComposeResolverHandler } from '../../modules/core/commands'
import { CommandInput, EventStoreCommit } from '../../modules/core/domain'

export default () => {
  return (handler: ComposeResolverHandler) => (next: any) => async (command: CommandInput): Promise<EventStoreCommit> => {
    let result = await next(command)
    if (typeof result === 'function') {
      result = await result(handler)
    }

    return result
  }
}
