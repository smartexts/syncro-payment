import ses from 'node-ses'

export default ({ config }) => {

  const client = ses.createClient(config.ses)

  return {
    async sendEmail (
		from: string, to: string[], subject: string, message: string,
		altText: string = '', cc: string[] = [], bcc: string[] = []
	): Promise<any> {
      client.sendEmail({
        to,
        from,
        cc,
        bcc,
        subject,
        message,
        altText
      }, (err, data) => {
        if (err) return Promise.reject(err)
        return Promise.resolve(data)
      })
    },
    sendAdminEmail (
		to: string[] = [], subject: string, message: string,
		altText: string = '', cc: string[] = [], bcc: string[] = []
	): Promise<any> {
      return new Promise((resolve, reject) => {
        client.sendEmail({
          from: 'no-reply@syncro.space',
          to,
          cc,
          bcc,
          subject,
          message,
          altText
        }, (err, data) => {
          if (err) return reject(err)
          return resolve(data)
        })
      })
    }
  }
}
