import { Database } from '../../domain/types'
import { JSONDB } from '../jsondb/JsonDB'
import neo4j from '../neo4j'

export default ({ config }): Database => {

  const db: Database = {
    jsondb: new JSONDB(),
  }

  if (config.database.neo4j) {
    db.neo4j = neo4j({ config: config.database })
  }

  return db
}
