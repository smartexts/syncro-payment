export default () => {
  const isPromise = obj => Boolean(obj) && typeof obj.then === 'function'

  const next = (iter, callback, prev = undefined) => {
    let item

    try {
      item = iter.next(prev)
    } catch (err) {
      return callback(err)
    }

    const value = item.value

    if (item.done) return callback(null, prev)

    if (isPromise(value)) {
      value.then(val => {
        setImmediate(() => next(iter, callback, val))
      }).catch(err => {
        return callback(err)
      })
    } else {
      setImmediate(() => next(iter, callback, value))
    }
  }

  const gensync = (fn) =>
    (...args) => new Promise((resolve, reject) => {
      next(fn(...args), (err, val) => {
        if (err) {
          reject(err)
        }
        resolve(val)
      })
    })

  return gensync
}
