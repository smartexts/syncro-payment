
class Neo4jDBHelper {
  cleanNode (node) {
    return {
      ...this.cleanNodeProperties(node.properties),
      _id: node.identity.toNumber(),
      _labels: node.labels,
    }
  }

  cleanNodeProperties (properties) {
    Object.keys(properties).forEach((key) => {
      if (properties[key] !== null) {
        if (typeof properties[key].toNumber === 'function') {
          properties[key] = properties[key].toNumber()
        } else if (typeof properties[key] === 'object') {
          if (properties[key].properties) {
            properties[key] = this.cleanNode(properties[key])
          } else {
            properties[key] = this.cleanNodeProperties(properties[key])
          }
        }
      }
    })

    return properties
  }

  cleanResult (results, keepArray = false) {
    const clean = (result) => {
      Object.keys(result).forEach((item) => {
        if (result[item] !== null) {
          if (result[item].properties) {
            result[item] = this.cleanNode(result[item])
          } else {
            result[item] = this.cleanNodeProperties(result[item])
          }

          // Remove key if result has one key only
          if (!keepArray && Object.keys(result).length === 1) {
            result = result[item]
          }
        }
      })

      return result
    }

    return results.map((result) => clean(result))
  }

  cleanValues (values) {
    if (typeof values === 'object' && values !== null) {
      Object.keys(values).forEach((key) => {
        if (typeof values[key] === 'object') {
          delete values[key]
        }
      })
    }

    return values
  }

  logQuery (options) {
    function log (option) {
      console.info(
        '\x1b[35m', '\n:params',
        '\x1b[35m', option.params,
        '\x1b[36m', `\n${option.query}`,
      )
    }

    if (typeof options.queries === 'object') {
      options.queries.forEach((option) => {
        log(option)
      })
    } else {
      log(options)
    }
  }

  buildParams (params, variable = true) {
    const _params = []

    Object.keys(params).forEach((key) => {
      if (variable) {
        if (typeof params[key] === 'object' && params[key] !== null) {
          Object.keys(params[key]).forEach((k) => {
            _params.push(`${k} : ${this.parameter(key, { [k]: params[key][k] }, 0)}`)
          })
        } else {
          _params.push(`${key} : ${this.parameter(key, params[key], 0)}`)
        }
      } else {
        _params.push(`${key}:"${params[key]}"`)
      }
    })

    return _params.length ? `{${_params.join(',')}}` : ''
  }

  buildUpdateParams (alias, params, set = true) {
    const _params = []

    Object.keys(params).forEach((key) => {
      if (typeof params[key] === 'object' && params[key] !== null) {
        Object.keys(params[key]).forEach((k) => {
          _params.push(`${alias}.${k} = ${this.parameter(key, { [k]: params[key][k] }, 0)}`)
        })
      } else {
        _params.push(`${alias}.${key} = ${this.parameter(key, params[key], 0)}`)
      }
    })

    return _params.length ? `${set ? 'SET ' : ''} ${_params.join(',')}` : ''
  }

  buildWhereCondition (params, alias) {
    const condition = []
    Object.keys(params).forEach((key) => {
      if (key === '_id') {
        condition.push(`id(${alias}) = {_id}`)
      } else {
        condition.push(`${alias}.${key} = ${this.parameter(key, params[key], 0)}`)
      }
    })

    return condition.join(' AND ')
  }

  parameter (key, value, level = 0) {
    let name = level > 0 ? `.${key}` : `$${key}`

    if (value !== null && typeof value === 'object') {
      Object.keys(value).forEach((k) => {
        name += this.parameter(k, value[k], level + 1)
      })
    }

    return name
  }
}

export default new Neo4jDBHelper()
