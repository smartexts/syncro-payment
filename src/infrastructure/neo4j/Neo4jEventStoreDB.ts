import { IEventStoreDB } from '../../modules/core/domain'
import { Neo4jDB } from './Neo4jDB'

export class Neo4jEventStoreDB extends Neo4jDB implements IEventStoreDB {

  async load (entity: string, filter: object): Promise<any[]> {
    const results = await this.findAll(entity, filter)

    if (results.length) {
      return results.map(r => {
        r.events = JSON.parse(r.events)
        return r
      })
    }

    return []
  }

  append (entity: string, data: any): Promise<any> {
    data.events = JSON.stringify(data.events)
    return this.create(entity, data)
  }

}
