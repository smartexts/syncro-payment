export default class Neo4jQueryBuilder {
  orderMatch = true

  _match: any = []
  _with: any = []
  _where: any = []
  _create: any = []
  _merge: any = []
  _update: any = []
  _order: any = []
  _delete: any = []
  _return: {} = {}
  _skip: any = 0
  _limit: any = 0
  _cacheable: any
  _params: {
    [key: string]: any
  } = {}

  _returnTypes: {
    [key: string]: Function
  } = {
    list (returns: any, alias: string): string {
      if (returns.length <= 0) {
        throw new Error('Query type normal must have at least one value!')
      }
      return `apoc.map.mergeList([ ${returns.join()} ]) AS ${alias}`
    },
    normal (returns: any, alias: string): string {
      if (returns.length > 1 || returns.length === 0) {
        throw new Error('Query type normal must have one value for one alias only!')
      }
      return alias ? `${returns[0]} AS ${alias}` : `${returns[0]}`
    },
  }
  _returnTypeMap: {
    [key: string]: string
  } = {}

  static init () {
    return new Neo4jQueryBuilder()
  }

  addReturnType (type, func) {
    this._returnTypes[type] = func
    return this
  }

  match (match: string, top = false, force = false): Neo4jQueryBuilder {
    match = match.trim()
    if (!match.match(/MATCH/) && !force) {
      throw new Error(`Error on add match query(${match}), query string must contain 'MATCH'`)
    }
    if (top) {
      this._match.unshift(match)
    } else {
      this._match.push(match)
    }
    return this
  }

  with (w: string) {
    this._with.push(w)
    return this
  }

  where (w: string) {
    this._where.push(w)
    return this
  }

  merge (merge: string) {
    this._merge.push(merge)
    return this
  }

  create (create: string) {
    this._create.push(create)

    return this
  }

  update (update: string) {
    this._update.push(update)

    return this
  }

  order (order: string) {
    this._order.push(order)

    return this
  }

  delete (del: string) {
    this._delete.push(del)
    return this
  }

  skip (value: number) {
    this._skip = (value < 0) ? 0 : value

    return this
  }

  limit (value: number) {
    this._limit = (value < 0) ? 0 : value

    return this
  }

  return (ret: string, alias = 'res', type = 'list') {
    if (typeof this._return[alias] === 'undefined') {
      // Type can be set at the first time only
      this._returnTypeMap[alias] = type
      // Init return
      this._return[alias] = [ret]
    } else {
      this._return[alias].push(ret)
    }

    return this
  }

  normalReturn (ret: string, alias = '') {
    return this.return(ret, alias, 'normal')
  }

  params (params: Object) {
    this._params = { ...this._params, ...params }
    return this
  }

  cacheable (options: any) {
    this._cacheable = options
    return this
  }

  mergeQuery (qc: Neo4jQueryBuilder) {
    this._match = [...this._match, ...qc._match]
    this._merge = [...this._merge, ...qc._merge]
    this._create = [...this._create, ...qc._create]
    this._update = [...this._update, ...qc._update]
    this._with = [...this._with, ...qc._with]
    this._where = [...this._where, ...qc._where]
    this._delete = [...this._delete, ...qc._delete]
    this._order = [...this._order, ...qc._order]
    // Consider to merge skip/limit
    this._skip = qc._skip
    this._limit = qc._limit
    this._params = { ...this._params, ...qc._params }

    // Meger return
    Object.keys(qc._return).forEach((alias) => {
      if (typeof this._return[alias] === 'undefined') {
        this._return[alias] = []
        this._returnTypeMap[alias] = qc._returnTypeMap[alias]
      } else if (this._returnTypeMap[alias] !== qc._returnTypeMap[alias]) {
        throw new Error(`Error on merge query, return (${this._returnTypeMap[alias]}) not match!!!`)
      }
      if (qc._return[alias].length) this._return[alias] = [...this._return[alias], ...qc._return[alias]]
    })

    return this
  }

  getTotal (node) {
    this._return = {}
    this._skip = false
    this._limit = false

    return this.normalReturn(`COUNT (${node})`)
  }

  buildReturnString (): string {
    const returns = []

    Object.keys(this._return).forEach((alias) => {
      if (typeof this._returnTypes[this._returnTypeMap[alias]] === 'undefined') {
        throw new Error(`Error on build query result, return type(${this._returnTypeMap[alias]}) not found!` +
          `Maybe forgot to addReturnType(${this._returnTypeMap[alias]}, Funtion)?`)
      }
      returns.push(this._returnTypes[this._returnTypeMap[alias]](this._return[alias], alias))
    })

    return returns.join(', ')
  }

  toString () {
    // We should re-order match query
    if (this.orderMatch) {
      this._match = this._match.sort((valueOld, valueNew) => {
        const rankNew = valueNew.match(/^OPTIONAL/) ? 1 : 0
        const rankOld = valueOld.match(/^OPTIONAL/) ? 1 : 0
        return rankOld - rankNew
      })
    }

    this._match = this._match.filter((el, i, arr) => (arr.indexOf(el) === i))
    this._merge = this._merge.filter((el, i, arr) => (arr.indexOf(el) === i))
    this._create = this._create.filter((el, i, arr) => (arr.indexOf(el) === i))
    this._update = this._update.filter((el, i, arr) => (arr.indexOf(el) === i))
    this._with = this._with.filter((el, i, arr) => (arr.indexOf(el) === i))
    this._order = this._order.filter((el, i, arr) => (arr.indexOf(el) === i))
    this._where = this._where.filter((el, i, arr) => (arr.indexOf(el) === i))
    this._delete = this._delete.filter((el, i, arr) => (arr.indexOf(el) === i))

    return [
      this._match.join('\n'),
      (this._merge.length ? `MERGE ${this._merge.join(', MERGE ')}` : ''),
      (this._create.length ? `CREATE ${this._create.join(',')}` : ''),
      (this._update.length ? `SET ${this._update.join(',')}` : ''),
      (this._with.length ? `WITH ${this._with.join(',')}` : ''),
      (this._order.length ? `ORDER BY ${this._order.join(',')}` : ''),
      (this._where.length ? `WHERE ${this._where.join(' AND ')}` : ''),
      (this._delete.length ? `DETACH DELETE ${this._delete.join(',')}` : ''),
      (Object.keys(this._return).length ?
        `RETURN ${this.buildReturnString()}${(this._skip ? ` SKIP ${this._skip}` : '')}${(this._limit ?
          ` LIMIT ${this._limit}` : '')}` : ''),

    ].filter((value) => (value !== '')).join('\n')
  }

  getQuery () {
    return { query: this.toString(), params: this._params, cacheable: this._cacheable }
  }
}
