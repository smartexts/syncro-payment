import { v1 as neo4j } from 'neo4j-driver'
import { Neo4jEventStoreDB } from './Neo4jEventStoreDB'
import { Neo4jDB } from './Neo4jDB'
import QueryBuilder from './Neo4jQueryBuilder'

export default ({ config }) => {
  const driver = neo4j.driver(config.neo4j.host, neo4j.auth.basic(config.neo4j.username, config.neo4j.password))
  return {
    eventstore: new Neo4jEventStoreDB(driver, config),
    db: new Neo4jDB(driver, config),
    builder: new QueryBuilder(),
  }
}
