import { v1 as uuid } from 'uuid'
import * as neo4j from 'neo4j-driver'
import helper from './Neo4jDBHelper'

export class Neo4jDB {
  constructor (protected db: neo4j.v1.Driver, protected config) {
  }

  findOne (node, params) {
    if (typeof params !== 'object') {
      params = { id: params }
    }

    return this.queryOne({
      query: `MATCH (n:${node} ${helper.buildParams(params)}) RETURN n`,
      params,
    })
  }

  findAll (node, params): Promise<any> {
    return this.find(node, params)
  }

  find (node, params) {
    if (typeof params !== 'object') {
      params = { id: params }
    }

    return this.query({
      query: `MATCH (n:${node} ${helper.buildParams(params)}) RETURN n`,
      params,
    })
  }

  findRelate (node, params, relate) {
    const direction = relate.direction || 'OUT'

    if (typeof params !== 'object') {
      params = { id: params }
    }

    const query = {
      query: [
        `MATCH (n:${node} ${helper.buildParams(params)})`,
        (direction === 'OUT' ? `MATCH (n)-[:${relate.rel}]->(relate)` : `MATCH (n)<-[:${relate.rel}]-(relate)`),
        'RETURN relate',
      ].join(' '),
      params,
    }

    return relate.many ? this.query(query) : this.queryOne(query)
  }

  async create (node, values): Promise<any> {
    // Add uuid by default
    if (!values.id) {
      values.id = uuid()
    }
    if (!values.created) {
      values.created = Math.round(+new Date() / 1000)
    }

    // Remove object params
    values = helper.cleanValues(values)

    const result = await this.queryOne({
      query: `CREATE (n:${node} ${helper.buildParams(values)})  RETURN n`,
      params: values,
    })
    return result

  }

  update (node, params, values: any = {}) {
    if (typeof params !== 'object') {
      params = { id: params }
    }
    if (!values.updated) {
      values.updated = Math.round(+new Date() / 1000)
    }
    values = helper.cleanValues(values)

    return this.queryOne({
      query: `MATCH (n:${node} ${helper.buildParams({ params })}) ${helper.buildUpdateParams('n', { values })} RETURN n`,
      params: { params, values },
    })
  }

  remove (node, params) {
    if (typeof params !== 'object') {
      params = { id: params }
    }
    return this.queryOne({
      query: `MATCH (n:${node} ${helper.buildParams(params)}) DETACH DELETE n`,
      params,
    })
  }

  async createRelate (fromNode: { node: string, params: any }, toNode: { node: string, params: any }, relate, replace = false) {
    if (replace) {
      await this.removeRelate(fromNode, toNode, relate)
    }
    const fromParams = helper.cleanValues(fromNode.params)
    const toParams = helper.cleanValues(toNode.params)
    const relateParams = helper.cleanValues(relate.values) || {}
    const direction = relate.direction || 'OUT'

    // Check to create relate node if not exist
    // Todo: check for special case
    if (!toParams.id && !toParams.username && !toParams.alias) {
      // let toNodeData = await this.queryOne({
      //     query: `MATCH (to:${toNode.node} ${helper.buildParams(toParams)}) RETURN to`,
      //     params: toParams
      // });
      // related node not found
      // if (!toNodeData) {
      const toNodeData = await this.create(toNode.node, toParams)
      toParams.id = toNodeData.id
      // }
    }

    const left = direction === 'IN' ? '<-' : '-'
    const right = direction === 'OUT' ? '->' : '-'

    return await this.queryOne({
      query: [
        `MATCH (from:${fromNode.node} ${helper.buildParams({ fromParams })})`,
        `MATCH (to:${toNode.node} ${helper.buildParams({ toParams })})`,
        // replace ? `OPTIONAL MATCH (from)${left}[rel:${relate.rel}]${right}(:${toNode.node})` : '',
        `MERGE (from)${left}[:${relate.rel} ${helper.buildParams({ relateParams })}]${right}(to)`,
        // replace ? 'DELETE rel' : '',
        'RETURN to',
      ].join(' '),
      params: {
        fromParams,
        toParams,
        relateParams,
      },
    })
  }

  removeRelate (fromNode, toNode, relate) {
    const fromParams = fromNode.params
    const toParams = toNode.params || {}
    const relateParams = relate.values || {}
    const direction = relate.direction || 'OUT'

    const left = direction === 'IN' ? '<-' : '-'
    const right = direction === 'OUT' ? '->' : '-'

    return this.queryOne({
      query: [
        `MATCH (from:${fromNode.node} ${helper.buildParams({ fromParams })})`,
        `MATCH (to:${toNode.node} ${helper.buildParams({ toParams })})`,
        `OPTIONAL MATCH (from)${left}[rel:${relate.rel} ${helper.buildParams({ relateParams })}]${right}(to)`,
        'DELETE rel',
        'RETURN to',
      ].join(' '),
      params: {
        fromParams,
        toParams,
        relateParams,
      },
    })
  }

  query (options = {}): Promise<any> {
    return this.run(options)
  }

  queryOne (options = {}): Promise<any> {
    return this.run(options).then((results) => results[0])
  }

  run (options: any = {}) {
    return this._run(options)
  }

  async _run (options: any = {}) {
    if (this.config.env === 'development') helper.logQuery(options)
    return new Promise((resolve, reject) => {
      const session = this.db.session()
      return session
        .run(options.query, options.params).then((result) => {
          // New clean method - will remove all method soon
          const records = helper.cleanResult(result.records.map((record) => record.toObject()))

          if (!records.length) {
            if (this.config.env === 'development') {
              console.info('\x1b[33m', '\n\nQUERY NO RESULT:')
              helper.logQuery(options)
            }
          }
          resolve(records)
          session.close()
        })
        .catch((error) => {
          console.error('\x1b[31m', '\n\nQUERY ERROR')
          helper.logQuery(options)
          reject(error)
        })
    })
  }
}
