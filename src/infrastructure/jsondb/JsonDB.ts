import * as fs from 'fs'
import * as path from 'path'
import * as _ from 'lodash'
import { IEventStoreDB } from '../../modules/core/domain'

export class JSONDB implements IEventStoreDB {
  private _directory: string

  constructor (directory = 'jsondb') {
    this._directory = directory
    if (!fs.existsSync(this._directory)) {
      fs.mkdirSync(this._directory)
    }
  }

  async load (entity: string, filter: object= {}): Promise<any[]> {
    let items = this._load(entity)

    if (items.length && Object.keys(filter).length) {
      return _.filter(items, filter)
    }

    return items
  }

  async append (entity, data: any): Promise<any[]> {
    if (!data) {
      throw new Error('Blank data exception')
    }

    const items = this._load(entity)
    items.push(data)
    fs.writeFileSync(path.join(this._directory, `${entity}.json`), JSON.stringify(items, null, 2))
    return data
  }

  _load (entity) {
    const _path = path.join(this._directory, `${entity}.json`)
    if (!fs.existsSync(_path)) {
      fs.writeFileSync(_path, JSON.stringify([]))
    }

    return JSON.parse(fs.readFileSync(_path).toString() || '[]')
  }
}
