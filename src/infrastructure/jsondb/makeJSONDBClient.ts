import { JSONDB } from './jsondb'
import { EventStore } from '../../modules/core/infrastructure'

export const makeJSONDBClient = (entityName): EventStore<JSONDB> => {
  return new EventStore(entityName, new JSONDB())
}
