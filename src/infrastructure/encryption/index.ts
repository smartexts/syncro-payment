import * as crypto from 'crypto'

type Password = {
  hash: string,
  salt: string
}

// generate hash password using salt
const genPassword = (value, salt): Password => ({
  hash: crypto.createHash('sha256').update(value + salt).digest('base64'),
  salt: salt.toString('hex')
})

// generate hash password using passed salt or random salt
const encryptPassword = (value, salt): Promise<Password> => new Promise((resolve, reject) => {
  if (typeof salt === 'string') {
    salt = Buffer.from(salt, 'hex') // eslint-disable-line
    resolve(genPassword(value, salt))
  } else {
    crypto.randomBytes(10, (err, _salt) => {
      if (err) {
        return reject(err)
      }
      return resolve(genPassword(value, _salt))
    })
  }
})

// check user password using input password, user salt;
const comparePassword = async (pass, { hash, salt }: Password) => {
  const encodePass = await encryptPassword(pass, salt)
  return encodePass.hash === hash
}

export {
    genPassword,
    encryptPassword,
    comparePassword
}
