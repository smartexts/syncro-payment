import * as fetch from 'node-fetch'

export default ({ jwt, config }) => {
  const signin = jwt.signin()

  function createToken (email) {
    return 'JWT ' + signin({ email })
  }

  const send = (type: 'command' | 'query', data, authorization) => {
    return new Promise((resolve, reject) => {
      fetch(config.eventsourcing.endpoint + '/' + type, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json', authorization }
      })
        .then(res => {
          // if (res.status !== 200) {
          //   reject([new Error('Command invalid')])
          // }
          return res.json()
        })
        .then(json => json)
        .then(data => {
          if (data.errors && data.errors.length) {
            reject(data.errors)
          }
          resolve(data)
        }).catch(e => {
          reject(e)
        })
    })
  }

  return (user) => {
    const authorization = createToken(user)

    return {
      query: (query) => {
        return send('query', { query }, authorization)
      },
      command: (command) => {
        return send('command', { command }, authorization)
      }
    }
  }
}
