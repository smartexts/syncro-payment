type IloggerLevel = 'info' | 'warn' | 'error'

export type ILogger = {
  log (level: IloggerLevel, message: string): void
  info (message: string): void
  warn (message: string): void
  error (message: string): void
}
