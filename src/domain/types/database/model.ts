export type Database = {
  [key: string]: any;
}

export default Database
